<?php  if ( ! defined('L_BASEPATH')) exit('No direct script access allowed');


class LView {

	var $params = array();

	var $first_run = true;

	function __construct($load){
		$this->loaded_LLoad = $load;
		$this->load = new LViewLoad($this);
	}	

	function render($name,$params=array()){

		$this->params = array_merge($this->params,$params);

		
			//load local variable
		foreach ($this->params as $key => $value) {

			${$key} = $value;
		}

		if($this->first_run){
			//load helper method
			$loaded_class =  $this->loaded_LLoad->getLoadedClass();
			foreach ($loaded_class as $key=>$value ) {

				$this->$key = &$loaded_class[$key];


			}
			$this->first_run = false;
		}
		

		//check if file exist

		set_error_handler(array($this,'errorHandler'));
		include L_BASEPATH."app/view/{$name}";
		restore_error_handler();




	}			

	 function errorHandler($errno, $errstr, $errfile, $errline) {

	 	
		if(!__c('debug')==true)
			return;

		$error_message='';
		if(__c('framework_debug'))	
			$error_message = $error_message."<p>Error On : <b>$errfile</b> line <b>$errline</b></p>";



		l_display_message('Gotcha ! Error found',"<p>$errstr</p>$error_message",'notice');
	}

}
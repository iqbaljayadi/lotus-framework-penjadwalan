<?php  if ( ! defined('L_BASEPATH')) exit('No direct script access allowed');

require_once L_BASEPATH.'lotus_core/l_view_load.php';

class LLoad extends LViewLoad {



	function __construct($LBase){

		parent::__construct($LBase);
		


	}

	function model($classname,$params=array()){
	
		$this->current_load_type='model';

		//set loading state
		$filename = camel_to_snake($classname)."_model";

		// @TODO remove "l_" prefix after namespace exist
		$classname = ucwords($classname)."Model";

		//loaded name start with lowercase 
		$loadedName = lcfirst($classname);

		$this->current_loaded_class = $classname;
		$this->load_path = L_BASEPATH."app/model/{$filename}.php";

		set_error_handler(array($this,'errorHandler'));
		@require_once $this->load_path;
		restore_error_handler();

		//test is class exist
		if(class_exists($classname)){

			//Create dynamic object
			$this->loaded_class[$loadedName] = new $classname($params);

			$this->LBase->$loadedName = &$this->loaded_class[$loadedName];



		}else{
			//@todo Throw error class not found
			$error_message = "<p>Class Not Found : <b>{$this->current_loaded_class}</b></p>";

			$this->displayError($error_message);		
		}
	
	}

}


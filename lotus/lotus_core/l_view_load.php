<?php  if ( ! defined('L_BASEPATH')) exit('No direct script access allowed');

require_once L_BASEPATH.'lotus_core/l_base_load.php';

class LViewLoad extends LBaseLoad {



	var $current_loaded_class;
	var $current_load_type;
	var $loaded_class = array();

	function __construct($LBase){

		parent::__construct($LBase);
		$this->LBase = $LBase;


	}

	

	/*
	 * Helper object are available on View / Helper by deffault
	 */

	function helper($classname,$params=array()){


		$filename = camel_to_snake($classname)."_helper";

		// @TODO remove "l_" prefix after namespace exist
		$classname = ucwords($classname)."Helper";

		//loaded name start with lowercase 
		$loadedName = lcfirst($classname);


		$this->load_path = L_BASEPATH."app/helper/{$filename}.php";

		$this->current_load_type='helper';
		$this->current_loaded_class = $classname;

		set_error_handler(array($this,'errorHandler'));
		@require_once $this->load_path;

		//if controller call than do not create object
		// array_push($this->loadedHelper, "{$name}Helper");  

		restore_error_handler();

		//test is class exist
		if(class_exists($classname)){

			//Create dynamic object
			$this->loaded_class[$loadedName] = new $classname($params);

		}else{
			//@todo Throw error class not found
			$error_message = "<p>Class Not Found : <b>{$this->current_loaded_class}</b></p>";

			$this->displayError($error_message);		
		}

		
		$this->LBase->$loadedName = &$this->loaded_class[$loadedName];
	}

	

	function getLoadedClass(){
		return $this->loaded_class;
	}


}


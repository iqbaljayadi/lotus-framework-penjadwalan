<?php



class LBaseLoad{

	function __construct($LObject){
		$this->LObject = $LObject;
	}

	function library($classname,$params=array()){
		
		//set loading state
		$filename = camel_to_snake($classname);


		//loaded name start with lowercase 
		$loadedName = lcfirst($classname);

		// @TODO remove "L" prefix after namespace exist
		$classname = "L".ucwords($classname);

		$this->current_load_type='library';

		$this->current_loaded_class = $classname;

		$this->load_path = '';

		if($classname!='LDb'){

			$this->load_path = L_BASEPATH."lotus_core/lib/{$filename}.php";

			//check file exist, if not exist try in app folder
			if(!file_exists($this->load_path))
				$this->load_path= L_BASEPATH."app/lib/{$filename}.php";
			
			if(!file_exists($this->load_path))
				$this->load_path= L_BASEPATH."app/lib/{$filename}.php";

			set_error_handler(array($this,'errorHandler'));
			@require_once $this->load_path;
			restore_error_handler();

			//test is class exist
			if(class_exists($classname)){

				//Create dynamic object
				$this->loaded_class[$loadedName] = new $classname($params);
				$this->LObject->$loadedName = &$this->loaded_class[$loadedName];
			}else{
				//@todo Throw error class not found
				$error_message = "<p>Class Not Found : <b>{$this->current_loaded_class}</b></p>";

				$this->displayError($error_message);			
			}

			return;

		}

		//database case
		$this->loaded_class['db'] = LotusFactory::getDb();
		$this->LObject->db = &$this->loaded_class['db'];
	}



	protected function errorHandler($errno, $errstr, $errfile, $errline) {

		if(!__c('debug')==true)
			return;

		$error_message = "<p>File Not Found : <b>{$this->load_path}</b></p>";		
		
		if(__c('framework_debug'))	
			$error_message = $error_message."<p>Error On : <b>$errfile</b> line <b>$errline</b></p>";

		$this->displayError($error_message);
	}

	protected function displayError($error_message){
		if($this->current_load_type=='library')
			l_display_message("Library '$this->current_loaded_class' load Failed","<p>Please check your file and classname, if it is a custom library please check /app/library/ folder. </p>$error_message",'notice');
		if($this->current_load_type=='model')
			l_display_message("'$this->current_loaded_class' load Failed","$error_message<p>Please check your file and classname on <b>{$this->load_path}</b>. </p>",'notice');	
		if($this->current_load_type=='helper')
			l_display_message("'$this->current_loaded_class' load Failed","$error_message<p>Please check your file and classname on <b>{$this->load_path}</b>. </p>",'notice');
	} 

}
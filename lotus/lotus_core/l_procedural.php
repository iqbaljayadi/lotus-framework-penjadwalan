<?php  if ( ! defined('L_BASEPATH')) exit('No direct script access allowed');

function l_start_clean(){
	//clean previous buffer, start new output
	ob_end_clean();
	ob_end_clean();

}

function l_start_json(){
	//clean previous buffer, start new output
	ob_end_clean();
	ob_end_clean();
	header('Content-Type: application/json');
}

function l_start_ajax(){
	//clean previous buffer, start new output
	ob_end_clean();
	ob_end_clean();
	header('Content-Type: application/javascript');
}

function print_e($text='', $return = false)
{
	$is_multiple = (func_num_args() > 2) ? true : false;
	if(!$is_multiple)
	{
		if(is_numeric($return))
		{
			if($return==1 || $return==0)
			{
				$return = $return ? true : false;
			}else $is_multiple = true;
		}
		if(!is_bool($return)) $is_multiple = true;
	}
	if($is_multiple)
	{
		echo "<pre style='text-align:left;'>\n";
		echo "<b>1 : </b>";
		print_r($text);
		$i = func_num_args();
		if($i > 1)
		{
			$j = array();
			$k = 1;
			for($l=1;$l < $i;$l++)
			{
				$k++;
				echo "\n<b>$k : </b>";
				print_r(func_get_arg($l));
			}
		}
		echo "\n</pre>";
	}else{
		if($return)
		{
			ob_start();
		}
		echo "<pre style='text-align:left;'>\n";
		print_r($text);
		echo "\n</pre>";
		if($return)
		{
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		}
	}
}

function l_display_message($title,$detail,$class){

	$title = $title;
	$detail = $detail;
	$class = $class;

	require_once L_BASEPATH."app/view/Message.php";
	

}

/* Create link to other pages
 *
 *  $rebuildQuery always safe the paramater for the hyperlink
 *
 *
 */

function l_base_p_url($controller,$method='index',$extra_params=array(),$rebuildQuery=array()){

	global $wp;

	if($wp && is_admin()){
		array_push($rebuildQuery,'page');
	}

	if($controller!='')
		$extra_params['controller']=$controller;

	if($method!='')
		$extra_params['method']=$method;

	return l_base_url('',$rebuildQuery,$extra_params);
}


function l_base_p_redirect($controller,$method='index',$rebuildQuery=array(),$extra_params=array()){

	global $wp;

	if($wp && is_admin()){
		array_push($rebuildQuery,'page');
	}

	$extra_params['controller']=$controller;
	$extra_params['method']=$method;

	l_base_redirect('',$rebuildQuery,$extra_params);
}

function l_base_url($url="",$rebuildQuery=array(),$extra_params=array()){
	$getQuery = "";



	if(sizeof($rebuildQuery)!=0){
		$part = parse_url($url);
		if(isset($part['query'])){

			$base_query = explode('&',$part['query']);

			$url= $part['path'];

			$final_base_query = array();

			//split each query part
			foreach ($base_query as $value) {
				$final_part = explode("=",$value);
				$final_base_query[$final_part[0]]=$final_part[1]; 
			}

			$extra_params = array_merge($extra_params,$final_base_query);

		}

		$additional_get = array();

		foreach ($rebuildQuery as $value) {
			
			if(isset($_GET[$value]))
				$additional_get[$value] = $_GET[$value];
		}

		$extra_params = array_merge($additional_get,$extra_params);

		$getQuery="?";

		$getQuery .= http_build_query($extra_params,'','&');
	}

	$base_url = LConfig::getConfig('base_url');

	//add back slash

	global $wp;

	if($wp && !is_admin()){

		if(substr($base_url, -1) != '/') {
			$base_url = $base_url."/";
		}
	}




	if($wp && !is_admin()){
		if($url!=''&&substr($url, -1) != '/'){ 
			$url = $url."/";
		}
	}


	if($getQuery=="?")
		return $base_url.$url;

	return $base_url.$url.$getQuery;
}

function l_assets_url($file){
	//test wp 
	global $wp;
	if(isset($wp)){
		return plugin_dir_url(L_BASEPATH)."lotus/assets/$file";
	}
	else{
		return l_base_url('asset/'.$file);
	}
}

function l_redirect($url){
	header("Location: $url");
	exit();
}


function l_base_redirect($url,$rebuildQuery=array(),$extra_params=array()){
	$getQuery = "";



	if(sizeof($rebuildQuery)!=0){
		$part = parse_url($url);

		if(isset($part['query'])){

			$base_query = explode('&',$part['query']);

			$url= $part['path'];

			$final_base_query = array();

			//split each query part
			foreach ($base_query as $value) {
				$final_part = explode("=",$value);
				$final_base_query[$final_part[0]]=$final_part[1]; 
			}

			$extra_params = array_merge($extra_params,$final_base_query);

		}

		$additional_get = array();

		foreach ($rebuildQuery as $value) {
			if(isset($_GET[$value]))
				$additional_get[$value] = $_GET[$value];
		}

		$extra_params = array_merge($additional_get,$extra_params);


		$getQuery="?";



		$getQuery .= http_build_query($extra_params,'','&');

	}

	$base_url = LConfig::getConfig('base_url');

	global $wp;

	//add back slash
	if($wp && !is_admin()){

		if(substr($base_url, -1) != '/') {
			$base_url = $base_url."/";
		}

	}

	//wp URL always end in backslash

	if($wp && !is_admin()){
		if(substr($url, -1) != '/') {
			$url = $url."/";
		}
	}

	$final_url = $base_url.$url.$getQuery;



	header("Location: $final_url");
	exit();
}



function l_bench_start(){
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];

	global $start;

	$start = $time;


}

function l_bench_stop(){

	global $start;

	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	return round(($finish - $start), 4);
}

function camel_to_snake($input) {
  preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
  $ret = $matches[0];
  foreach ($ret as &$match) {
    $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
  }
  return implode('_', $ret);
}

function __t($key,$key2=false){

	return LConfig::getTranslation($key,$key2);
}

function __c($key1,$key2=false){
	return LConfig::getConfig($key1,$key2);
}
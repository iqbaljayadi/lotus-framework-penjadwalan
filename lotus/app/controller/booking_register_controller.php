<?php

class Booking_registerController extends AutoloadController {

	function __construct(){
		parent::__construct();
		$this->load->model('user');

	}

	function index(){

		$data['flash'] = $this->data->getFlash('message');
		$data['errflash'] = $this->data->getFlash('errmessage');
		$data['title'] = 'Booking Account Registration';

		$this->view->render('booking_register_view.php',$data);
	}

	function register(){

		$creds					= array();
		$creds['jdv_id']		= $this->input->post('jdv_id',true);
		$creds['password']		= $this->input->post('password',true);
		$creds['email']			= $this->input->post('email',true);

		$jdv_user = $this->userModel->get_jdv_user("'".$creds['jdv_id']."'");

		if(!$jdv_user) {
			$this->data->setFlash('message',$this->jdvHelper->alert('danger','ID JDV anda tidak terdaftar.'),true);
			l_base_redirect('booking_register');
		}

		else if($jdv_user->email != $creds['email']) {
			$this->data->setFlash('message',$this->jdvHelper->alert('danger','Email yang anda masukkan harus sesuai dengan email pendaftaran ID JDV anda.'),true);
			l_base_redirect('booking_register');
		}

		else {
			global $reg_errors;
			$reg_errors = new WP_Error;

			if ( empty($creds['jdv_id']) || empty($creds['password']) || empty($creds['email']) ) {

				$reg_errors->add('field', 'Kolom username, password, dan email harus diisi.');
			}

			if ( username_exists( $creds['jdv_id'] ) ) {
				$reg_errors->add('user_name', 'Maaf, ID JDV anda telah terdaftar sebagai user booking.');

			}

			if ( ! validate_username( $creds['jdv_id'] ) ) {
				$reg_errors->add( 'username_invalid', 'Maaf, username yang anda masukkan tidak valid.' );

			}

			if ( 6 > strlen( $creds['password'] ) ) {
				$reg_errors->add( 'password', 'Panjang password minimal 6 karakter.' );

			}

			if ( !is_email( $creds['email'] ) ) {
				$reg_errors->add( 'email_invalid', 'Email yang anda masukkan tidak valid.' );
			}

			if ( email_exists( $creds['email'] ) ) {
				$reg_errors->add( 'email', 'Email sudah dipakai' );
			}



			if ( is_wp_error( $reg_errors ) ) {

				$err_msg = array();
				$count = 0;
				foreach ( $reg_errors->get_error_messages() as $error ) {
					$err_msg[$count] = $this->jdvHelper->alert('danger',$error);
					$count ++;
				}
			}

			if ( 1 > count($reg_errors->get_error_messages()) ) {

				$userdata = array(
					'user_login'    =>   sanitize_user($creds['jdv_id']),
					'user_email'    =>   sanitize_email($creds['email']),
					'user_pass'     =>   esc_attr($creds['password']),
					'first_name'    =>   sanitize_text_field($jdv_user->firstname),
					'last_name'     =>   sanitize_text_field($jdv_user->lastname)
					);

				$user_id = wp_insert_user( $userdata );
				add_user_meta( $user_id, 'jdv_user_id', $userdata['user_login']);

				$this->data->setFlash('message',$this->jdvHelper->alert('success','Selamat, akun booking anda telah dibuat! Silakan login menggunakan akun anda untuk melanjutkan pemesanan event.'),true);
				l_base_redirect('booking_login');
			}

			else {
				$this->data->setFlash('errmessage',$err_msg,true);
				l_base_redirect('booking_register');
			}
		}
	}
}
<?php
class AjaxController extends WPFrontEndController{

	function __construct(){
		parent::__construct();
		l_start_ajax();
		$this->load->library('input');
		$this->load->model('user');
		$this->load->model('event');
		$this->load->helper('jdv');
	}

	function get_conflicted_events() {
		$id = $this->input->post('id',true);
		$date = $this->input->post('date',true);

		// manggil query
		$event_conflict = $this->eventModel->get_same_day_event($id,$date);

		if($event_conflict) {
			$data = '<div class="row">
			<div id="list-same-schedule" class="col-sm-11">
				<span>There are '.count($event_conflict).' events with same day schedule.</span>
				<div class="row">';
					foreach ($event_conflict as $key => $conflict) {
						$data.='
						<div class="col-sm-12">
							<div class="same-schedule-item '.$this->jdvHelper->get_event_status($conflict->status).'">
								<div class="row">
									<div class="col-sm-4 same-schedule-event-name">
										<a target="_blank" href="'.l_base_url('admin_event/manage_event/'.$conflict->id).'">'.$conflict->event_name.'</a>
									</div>
									<div class="col-sm-4 same-schedule-room">'.$this->jdvHelper->get_event_room($conflict->room).'</div>
									<div class="col-sm-2 same-schedule-time">'.date('G:i',strtotime($conflict->start_time)).'&nbsp;-&nbsp;'.date('G:i',strtotime($conflict->end_time)).'</div>
									<div class="col-sm-2 same-schedule-status">'.$this->jdvHelper->get_event_status($conflict->status).'</div>
								</div>
							</div>
						</div>';
					}
					$data .='</div>
				</div>
			</div>';
			echo json_encode($data);
			die();
		}
	}

	function submit_comment() {
		$event_data = $this->eventModel->get_event($this->input->post('event_id',true));
		$data = array(
			'event_id'	=>array($this->input->post('event_id',true),'s'),
			'user_id' 	=>array(get_current_user_id(),'d'),
			'comment' 	=>array($this->input->post('comment',true),'s')
			);

		$comment = $this->input->post('comment',true);

		if((get_current_user_id() ==  $event_data->user_id)||current_user_can('install_plugins')) {

			// Insert data to database
			$comment_id = $this->eventModel->insert_comment($data);

			// get inserted comment
			$comment = $this->eventModel->get_comment($comment_id);

			// Email Notification
			$admin_email = get_option('admin_email');
			// $admin_email = 'muhiqbalj@gmail.com';
			
			if(current_user_can('install_plugins')) {
				$link 		= l_base_url('user_booking/manage_booking/'.$event_data->id);
				$to			= $event_data->email;
				$delete_link = l_base_url('admin_event/delete_comment').$comment_id;
			}
			else {
				$link 		= l_base_url('admin_event/manage_event/'.$event_data->id);
				$to			= $admin_email;
				$delete_link = l_base_url('user_booking/delete_comment').$comment_id;
			}

			/// Setup Email Message
			$subject	= 'Notifikasi Booking - Pesan Baru Pada '.$event_data->event_name;
			$message	= 'Anda mendapatkan pesan baru untuk booking event <strong>'
			.$event_data->event_name.'</strong>. Untuk melihat detail pesan silakan klik link berikut:
			'.$link;
			$headers	= 'From: Jogja Digital Valley <noreply@jogjadigitalvalley.com>';
			wp_mail( $to, $subject, $message, $headers);

			/// setup html for json return
			$user_info = get_userdata(get_current_user_id());
			$data = 
			'<li class="comment-item-'.$comment_id.'">
			<div class="comment-identity">

				<span class="comment-name">'.$user_info->first_name.' '.$user_info->last_name.'</span>
				&nbsp;
				<span class="comment-time">'.date('F j, Y H.i',strtotime($comment->timestamp)).'</span>
			</div>
			<div class="comment-content">'.$comment->comment.'</div>';
			$data .= '<div class="comment-delete"><div class="btn btn-mini btn-warning delete">Delete</div></div>';
			$data .= '</li>';
			if($comment_id)
			{
				$return['alert'] = 'Pesan berhasil dikirim!';
				$return['type'] = 'success';
			}else{
				$return['alert'] = 'Maaf, pesan gagal terkirim. Silakan coba lagi.';
				$return['type'] = 'danger';
			}
			$return['data'] = $data;
			echo json_encode($return);
			die();
		}
	}

	function delete_comment() {
		$user_id 	= get_current_user_id();
		$event_id	= $this->input->post('event_id',true);
		$comment_id = $this->input->post('comment_id',true);
		$comment_data = $this->eventModel->get_comment($comment_id);

		// Cek komentar yang akan dihapus merupakan kepunyaan user yang login
		if($user_id == $comment_data->user_id) {

		// Cek komentar yang akan dihapus adalah event yang bersangkutan
			if($event_id == $comment_data->event_id) {
				$this->eventModel->delete_comment($comment_id);
				$return['alert'] = 'Pesan berhasil dihapus!';
				$return['type'] = 'success';
			}

			else {
				$return['alert'] = 'Maaf, gagal menghapus pesan. Silakan coba lagi.';
				$return['type'] = 'danger';
			}
		}

		else {
			$return['alert'] = 'Maaf, anda tidak dapat menghapus pesan orang lain.';
			$return['type'] = 'danger';
		}

		echo json_encode($return);
		die;

	}
}
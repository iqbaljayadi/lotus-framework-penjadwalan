<?php
class WelcomeController extends AutoloadController{

	function __construct(){
		parent::__construct();

	}

	function index(){
		if(is_user_logged_in()) {
			if(current_user_can('install_plugins')) {
				l_base_redirect('admin_event');
			}

			else {
				l_base_redirect('user_booking');
			}
		}
		else {
			l_base_redirect('booking_login');
		}
	}
}
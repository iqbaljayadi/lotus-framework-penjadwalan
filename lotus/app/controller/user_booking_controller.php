<?php

class User_bookingController extends AutoloadController{

	function __construct(){
		parent::__construct();
		// $this->load->model('user');
		$this->load->model('event');
		// $this->load->helper('jdv');

		$this->jdvHelper->jdv_redirect_not_user();
	}

	function index(){

		$data['flash'] = $this->data->getFlash('message');
		$data['title'] = 'My Events';

		$orderBy = '';
		$room = '';
		$start_date = $this->input->get('start_date');
		$end_date = $this->input->get('end_date');
		$event_name = $this->input->get('event_name');
		$status = $this->input->get('status');
		$user_id = get_current_user_id();

		$data['events'] = $this->eventModel->get_all_events($orderBy,$room,$start_date,$end_date,$event_name,
			$status,$user_id);
		$this->view->render('user/booking_view.php',$data);
	}

	function new_booking(){
		$data['title'] = 'Request New Event';

			// get user's meta

		$data['userdata'] = $this->userdata;
		$data['usermeta'] = $this->usermeta;
		$this->view->render('user/new_booking_view.php',$data);
	}

	function submit_booking() {

			// Data Verification

		$start = date($this->input->post('start_time',true));
		$end = date($this->input->post('end_time',true));

		if($start>$end) {
			$this->data->setFlash('message',$this->jdvHelper->alert('danger','end time must be after start time'),true);

		// Redirect to admin event page
			l_base_redirect('admin_event/new_event');
		}

		$date = $this->input->post('date',true);

		if(is_numeric($date))
		{
			$date = date('Y-m-d',$date);
		}else{
			$date = date('Y-m-d',strtotime($date));
		}

			// Compile user input
		$data = array(
			'user_id' 			=>array(get_current_user_id(),'d'),
			'event_name' 		=>array($this->input->post('event_name',true),'s'),
			'name' 				=>array($this->input->post('creator_name',true),'s'),
			'email' 			=>array($this->input->post('email',true),'s'),
			'phone' 			=>array($this->input->post('phone',true),'s'),
			'date'				=>array($date,'s'),
			'start_time' 		=>array($start,'s'),
			'end_time'		 	=>array($end,'s'),
			'participant' 		=>array($this->input->post('participant',true),'d'),
			'notes' 			=>array($this->input->post('notes',true),'s'),
			'date_created' 		=>(string)date("Ymd")
			);

		// Insert data to database
		$id = $this->eventModel->insert_event($data);

		// Email Setup
		$user_name	= $this->input->post('creator_name',true);
		// $admin_email = 'muhiqbalj@gmail.com';

		$admin_email = get_option('admin_email');
		$link 		= l_base_url('admin_event/manage_event/'.$id);
		$to			= $admin_email;
		$subject	= 'Notifikasi Booking -  Event Baru "'.$this->input->post('event_name',true)."'";
		$message	= 'User '.$user_name.' membuat booking event baru dengan judul <strong>'
		.$this->input->post('event_name',true).'</strong>, untuk melihat detail booking silakan klik link berikut: '.$link;
		$headers	= 'From: Jogja Digital Valley <noreply@jogjadigitalvalley.com>';

		wp_mail( $to, $subject, $message, $headers);

		$this->data->setFlash('message',$this->jdvHelper->alert('success','New event created!'),true);

		// Redirect to user event page
		l_base_redirect('user_booking');
	}


	function manage_booking($id) {
		if($id) {
			$data['flash'] = $this->data->getFlash('message');

			$data['title'] = 'Manage Event';

			// get user's meta
			$data['usermeta'] = $this->usermeta;

			// get event data
			$data['event'] = $this->eventModel->get_event($id);

			// get comment data
			$data['comments'] = $this->eventModel->get_all_comments($id);
			$this->view->render('user/manage_booking_view.php',$data);
		}

		else {
			$this->data->setFlash('message',$this->jdvHelper->alert('error','Event not found'),true);
			l_base_redirect('user_booking');
		}
	}

		// this function apply to manage event only!!
	function update_manage_booking($id) {
		$event = $this->eventModel->get_event($id);

		if($id && $event) {
			$data = array(
				'event_name'	=>array($this->input->post('event_name',true),'s'),
				'phone' 		=>array($this->input->post('phone',true),'s'),
				'participant'	=>array($this->input->post('participant',true),'d'),
				'notes'			=>array($this->input->post('notes',true),'s'),
				);
			if((($event->event_name != $this->input->post('event_name',true)) || ($event->phone != $this->input->post('phone',true)))||(($event->notes != $this->input->post('notes',true)) || ($event->participant != $this->input->post('participant',true)))) {
					/// Setup Email Message

				// $admin_email = 'muhiqbalj@gmail.com';
				$admin_email = get_option('admin_email');
				$link 		= l_base_url('admin_event/manage_event/'.$id);
				$to			= $admin_email;
				$subject	= 'Notifikasi Booking - '.$event->event_name;
				$message	= $event->name.' mengubah detail booking <strong>'.$event->event_name.'</strong>. Untuk melihat selengkapnya silakan klik link berikut: '.$link;
				$headers	= 'From: Jogja Digital Valley <noreply@jogjadigitalvalley.com>';
				wp_mail( $to, $subject, $message, $headers);
			}

			$this->eventModel->update_event($id,$data);

			$this->data->setFlash('message',$this->jdvHelper->alert('success','Event successfully updated!'),true);

			// Redirect to manage event page
			l_base_redirect('user_booking/manage_booking/'.$id);
		}

		else {
			$this->data->setFlash('message',$this->jdvHelper->alert('error','Event not found'),true);
			l_base_redirect('user_booking');
		}
	}

	function submit_comment($id) {

		if($id) {
			$data = array(
				'event_id'	=>array($id,'d'),
				'user_id' 	=>array(get_current_user_id(),'d'),
				'comment' 	=>array($this->input->post('comment',true),'s')
				);

			// Insert data to database
			$this->eventModel->insert_comment($data);

			// Email Notification
			$admin_email = get_option('admin_email');
			// $admin_email = 'muhiqbalj@gmail.com';
			$event_data = $this->eventModel->get_event($id);

			/// Setup Email Message
			$link 		= l_base_url('admin_event/manage_event/'.$id);
			$to			= $admin_email;
			$subject	= 'Notifikasi Booking - '.$event_data->event_name.' - Pesan Baru';
			$message	= $event_data->name.' mengirim pesan baru pada booking event <strong>'
			.$event_data->event_name.'</strong>. Untuk melihat selengkapnya silakan buka link berikut: '.$link;
			$headers	= 'From: Jogja Digital Valley <noreply@jogjadigitalvalley.com>';
			wp_mail( $to, $subject, $message, $headers);

			$this->data->setFlash('message',$this->jdvHelper->alert('success','Comment successfully posted!'),true);  

			// Redirect to Manage event page
			l_base_redirect('user_booking/manage_booking/'.$id);
		}

		else {
			l_base_redirect('user_booking/');
		}
	}

	function delete_booking($id) {
		$event = $this->eventModel->get_event($id);
		if($event) {

			// Memastikan user menghapus eventnya sendiri
			if(get_current_user_id() == $event->user_id) {
				$this->eventModel->delete_event($id);

				$this->data->setFlash('message',$this->jdvHelper->alert('success','Event booking berhasil dihapus!'),true);  
				l_base_redirect('user_booking/');
			}
		}

		else {
			$this->data->setFlash('message',$this->jdvHelper->alert('danger','Gagal menghapus event booking. Silakan coba lagi.'),true);
			l_base_redirect('user_booking/');
		}
	}
}
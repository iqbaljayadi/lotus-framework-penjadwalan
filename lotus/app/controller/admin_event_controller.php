<?php

class Admin_eventController extends AutoloadController{

	function __construct(){
		parent::__construct();
		$this->load->model('user');
		$this->load->model('event');
		// $this->load->helper('jdv');

		$this->jdvHelper->jdv_redirect_not_admin();
	}

	function index(){

		$data['flash'] = $this->data->getFlash('message');
		$data['title'] = 'Event List';

		$start_date = $this->input->get('start_date');
		$end_date = $this->input->get('end_date');

		if($start_date) {
			if(is_numeric($start_date)) {
				$start_date = date('Y-m-d',$start_date);
			}
			else {
				$start_date = date('Y-m-d',strtotime($start_date));
			}
		}

		if($end_date) {
			if(is_numeric($end_date)) {
				$end_date = date('Y-m-d',$end_date);
			}
			else {
				$end_date = date('Y-m-d',strtotime($end_date));
			}
		}

		$orderBy = $this->input->get('orderBy');
		$room = $this->input->get('room');
		$event_name = $this->input->get('event_name');
		$status = $this->input->get('status');

		$data['events'] = $this->eventModel->get_all_events($orderBy,$room,$start_date,$end_date,$event_name,
			$status);
		$this->view->render('admin/event_view.php',$data);
	}

	function new_event(){

		$data['flash'] = $this->data->getFlash('message');
		$data['title'] = 'New Event';

		// get user's meta
		$data['usermeta'] = $this->usermeta;
		$this->view->render('admin/new_event_view.php',$data);
	}

	function edit_event($id) {
		if($id) {
			$data['flash'] = $this->data->getFlash('message');
			$data['title'] = 'Edit Event';

			// get user's meta
			$data['usermeta'] = $this->usermeta;

			// get event data
			$data['event'] = $this->eventModel->get_event($id);
			$this->view->render('admin/edit_event_view.php',$data);
		}
		else {
			l_base_redirect('admin_event');
		}
	}

	function manage_event($id) {
		if($id) {
			$data['flash'] = $this->data->getFlash('message');

			$data['title'] = 'Manage Event';

			// get user's meta
			$data['usermeta'] = $this->usermeta;

			// get event data
			$data['event'] = $this->eventModel->get_event($id);

			if($data['event']) {

				$date = $data['event']->date;
			// get conflicted event data
				$data['event_conflict'] = $this->eventModel->get_same_day_event($id,$date);

			// get comment data
				$data['comments'] = $this->eventModel->get_all_comments($id);
				$this->view->render('admin/manage_event_view.php',$data);
			}

			else {
				$this->data->setFlash('message',$this->jdvHelper->alert('danger','Event tidak ditemukan'),true);
				l_base_redirect('admin_event');
			}
		}

		else {
			$this->data->setFlash('message',$this->jdvHelper->alert('danger','Event tidak ditemukan'),true);
			l_base_redirect('admin_event');
		}
	}

	// this function apply to create new event only!!
	function submit_event() {

			// Data Verification

		$start = date($this->input->post('start_time',true));
		$end = date($this->input->post('end_time',true));

		if($start>$end) {
			$this->data->setFlash('message',$this->jdvHelper->alert('danger','end time must be after start time'),true);

		// Redirect to admin event page
			l_base_redirect('admin_event/new_event');
		}

		else {

			$date = $this->input->post('date',true);

			if(is_numeric($date))
			{
				$date = date('Y-m-d',$date);
			}else{
				$date = date('Y-m-d',strtotime($date));
			}

			$data = array(
				'user_id' 			=>array(get_current_user_id(),'d'),
				'event_name' 		=>array($this->input->post('event_name',true),'s'),
				'name' 				=>array($this->input->post('creator_name',true),'s'),
				'email' 			=>array($this->input->post('email',true),'s'),
				'phone' 			=>array($this->input->post('phone',true),'s'),
				'room' 				=>array($this->input->post('room',true),'d'),
				'date'				=>array($date,'s'),
				'start_time' 		=>array($this->input->post('start_time',true),'s'),
				'end_time'		 	=>array($this->input->post('end_time',true),'s'),
				'event_manager' 	=>array($this->input->post('event_manager',true),'s'),
				'participant' 		=>array($this->input->post('participant',true),'d'),
				'status' 			=>array($this->input->post('status',true),'d'),
				'notes' 			=>array($this->input->post('notes',true),'s'),
				'date_created' 		=>(string)date("Ymd")
				);

		// Insert data to database
			$id = $this->eventModel->insert_event($data);

			$this->data->setFlash('message',$this->jdvHelper->alert('success','New event created!'),true);

		// Redirect to admin event page
			l_base_redirect('admin_event');
		}
	}

	// this function apply to edit event only!!
	function update_event($id) {
		if($id) {

			$start = date($this->input->post('start_time',true));
			$end = date($this->input->post('end_time',true));

			if($start>$end) {
				$this->data->setFlash('message',$this->jdvHelper->alert('danger','end time must be after start time'),true);

			// Redirect to admin event page
				l_base_redirect('admin_event/edit_event/'.$id);
			}

			else {

				$date = $this->input->post('date',true);

				if(is_numeric($date))
				{
					$date = date('Y-m-d',$date);
				}else{
					$date = date('Y-m-d',strtotime($date));
				}

				$data = array(
					'event_name' 		=>array($this->input->post('event_name',true),'s'),
					'name' 				=>array($this->input->post('creator_name',true),'s'),
					'email' 			=>array($this->input->post('email',true),'s'),
					'phone' 			=>array($this->input->post('phone',true),'s'),
					'room' 				=>array($this->input->post('room',true),'d'),
					'date'				=>array($this->input->post('date',true),'s'),
					'start_time' 		=>array($date,'s'),
					'end_time'		 	=>array($this->input->post('end_time',true),'s'),
					'event_manager' 	=>array($this->input->post('event_manager',true),'s'),
					'participant' 		=>array($this->input->post('participant',true),'d'),
					'status' 			=>array($this->input->post('status',true),'d'),
					'notes' 			=>array($this->input->post('notes',true),'s'),
					);

				$this->eventModel->update_event($id,$data);

				$this->data->setFlash('message',$this->jdvHelper->alert('success','Event successfully updated!'),true);

			// Redirect to manage event page
				l_base_redirect('admin_event/edit_event/'.$id);
			}
		}

		else {
			l_base_redirect('admin_event/event/edit_event/'.$id);
		}
	}

	// this function apply to manage event only!!
	function update_manage_event($id) {
		if($id) {

			$start = date($this->input->post('start_time',true));
			$end = date($this->input->post('end_time',true));

			if($start>$end) {
				$this->data->setFlash('message',$this->jdvHelper->alert('danger','end time must be after start time'),true);

		// Redirect to admin event page
				l_base_redirect('admin_event/manage_event/'.$id);
			}

			else {


				$date = $this->input->post('date',true);

				if(is_numeric($date))
				{
					$date = date('Y-m-d',$date);
				}else{
					$date = date('Y-m-d',strtotime($date));
				}

				$data = array(
					'date'				=>array($date,'s'),
					'start_time' 		=>array($this->input->post('start_time',true),'s'),
					'end_time'		 	=>array($this->input->post('end_time',true),'s'),
					'room'				=>array($this->input->post('room',true),'d'),
					'event_manager' 	=>array($this->input->post('event_manager',true),'s'),
					'status' 			=>array($this->input->post('status',true),'d')
					);

				$event_data = $this->eventModel->get_event($id);

				if ($event_data->status != $this->input->post('status',true)) {

				/// Setup Email Message
					$link 		= l_base_url('user_booking/manage_booking/'.$id);
					$to			= $event_data->email;
					$subject	= 'Notifikasi Booking - '.$event_data->event_name;
					$message	= 'Halo '.$event_data->name.', status booking event untuk <strong>'.$event_data->event_name.'</strong> berubah menjadi '
					.$this->jdvHelper->get_event_status($this->input->post('status',true)).'. Untuk melihat detail ubahan silakan klik link berikut: '.$link;
					$headers	= 'From: Jogja Digital Valley <noreply@jogjadigitalvalley.com>';
					wp_mail( $to, $subject, $message, $headers);
				}

				$this->eventModel->update_event($id,$data);

				$this->data->setFlash('message',$this->jdvHelper->alert('success','Event successfully updated!'),true);

			// Redirect to manage event page
				l_base_redirect('admin_event/manage_event/'.$id);
			}
		}

		else {
			l_base_redirect('admin_event');
		}
	}

	function delete_event($id) {
			// Get comment data
		$event = $this->eventModel->get_event($id);
		if($event) {

			// Make sure user can only delete his comment
			if((get_current_user_id() == $event->user_id)|| current_user_can('install_plugins')) {
				$this->eventModel->delete_event($id);
				$this->data->setFlash('message',$this->jdvHelper->alert('success','Event successfully deleted!'),true);
				l_base_redirect('admin_event');
			}

			else {
				$this->data->setFlash('message',$this->jdvHelper->alert('danger','Tidak dapat menghapus event'),true);
				l_base_redirect('admin_event');
			}
		}

		else {
			$this->data->setFlash('message',$this->jdvHelper->alert('danger','Event tidak ditemukan'),true);
			l_base_redirect('admin_event');

		}
	}

	// Thread things

	function submit_comment($id) {

		if($id) {
			$data = array(
				'event_id'	=>array($id,'d'),
				'user_id' 	=>array(get_current_user_id(),'d'),
				'comment' 	=>array($this->input->post('comment',true),'s')
				);

			// Insert data to database
			$this->eventModel->insert_comment($data);

			// Email Notification
			$admin_email = get_option('admin_email');
			$event_data = $this->eventModel->get_event($id);

			/// Setup Email Message
			$link 		= l_base_url('user_booking/manage_booking/'.$id);
			$to			= $event_data->email;
			$subject	= 'Notifikasi Booking - Pesan Baru Untuk '.$event_data->event_name;
			$message	= 'Anda mendapatkan pesan baru untuk booking event <strong>'
			.$event_data->event_name.'</strong>. Untuk melihat detail pesan silakan klik link berikut: '.$link;
			$headers	= 'From: Jogja Digital Valley <noreply@jogjadigitalvalley.com>';
			wp_mail( $to, $subject, $message, $headers);

			$this->data->setFlash('message',$this->jdvHelper->alert('success','Comment successfully posted!'),true);  

			// Redirect to Manage event page
			l_base_redirect('admin_event/manage_event/'.$id);
		}
		else {
			l_base_redirect('admin_event/');
		}
	}

	function delete_comment($id) {
			// Get comment data
		$comment = $this->eventModel->get_comment($id);
		if(!$comment) {

		}

			// Make sure user can only delete his comment
		if(get_current_user_id() == $comment->user_id) {
			$this->eventModel->delete_comment($id);
			$this->data->setFlash('message',$this->jdvHelper->alert('success','Comment successfully deleted!'),true);
			l_base_redirect('admin_event/manage_event/'.$comment->event_id);
		}

			// Else, wrath from the Code
		else {
			echo "YOU CANNOT DELETE OTHER PEOPLE'S COMMENT";
			die();
		}

	}

}
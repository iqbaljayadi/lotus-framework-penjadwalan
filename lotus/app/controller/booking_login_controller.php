<?php
class Booking_loginController extends AutoloadController {

	function __construct(){
		parent::__construct();

	}

	function index(){
		if(!is_user_logged_in()) {
			$data['flash']		= $this->data->getFlash('message');
			$data['title']		= 'Booking Login';
			$this->view->render('booking_login_view.php',$data);
		}

		else {
			l_base_redirect('welcome');
		}
	}
	

	function login_process() {
		$creds					= array();
		$creds['user_login']	= $_POST['log'];
		$creds['user_password'] = $_POST['pwd'];
		$creds['remember']		= false;
		$user 					= wp_signon( $creds, false );

		if ( is_wp_error($user) )
		{
    //Generate message on failed login
			$this->data->setFlash('message',$this->jdvHelper->alert('danger','ID JDV atau password anda salah'),true);   
			l_base_redirect('booking_login');
		}

		else {
			wp_redirect(l_base_url());
		}
	}
}
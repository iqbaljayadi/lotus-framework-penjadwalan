	<?php

	class eventModel extends LModel {

		public function get_all_events($orderBy='DESC',$room='',$start_date='',$end_date='',$event_name='',$status='',$user_id='') {
			$sql = '';
			if($room) {
				$sql .= "WHERE room = ".$room;
			}

			if ($start_date) {
				if(is_numeric($start_date)) {
					$start_date = date('Y-m-d',$start_date);
				}

				else{
					$start_date = date('Y-m-d',strtotime($start_date));				
				}

				if($sql){
					$sql .= " AND date <= '".$start_date."'";
				}

				else{
					$sql .= "WHERE date >= '".$start_date."'";
				}
			}

			if ($end_date) {
				if(is_numeric($end_date)) {
					$end_date = date('Y-m-d',$end_date);
				}

				else{
					$end_date = date('Y-m-d',strtotime($end_date));				
				}

				if($sql){
					$sql .= " AND date <= '".$end_date."'";
				}

				else{
					$sql .= "WHERE date <= '".$end_date."'";
				}
			}

			if($event_name) {
				if($sql){
					$sql .= " AND event_name LIKE '%".$event_name."%'";
				}

				else{
					$sql .= "WHERE event_name LIKE '%".$event_name."%'";
				}
			}

			if ($status!=NULL) {
				if($sql){
					$sql .= " AND status = ".$status;
				}

				else{
					$sql .= "WHERE status = ".$status;
				}
			}

			if (!current_user_can('install_plugins')) {
				if($sql){
					$sql .= " AND user_id = ".$user_id;
				}
				else{
					$sql .= "WHERE user_id = ".$user_id;
				}
			}

			if(!$orderBy) {
				$orderBy = 'DESC';
			}

			$query = "SELECT * FROM jdv_event  ".$sql." order by id ".$orderBy." ";

			$events = $this->db->getResults($query);
			return $events;
		}

		public function get_event($id){
			$this->db->where('id',$id);
			$event = $this->db->getSingle('jdv_event');
			return $event;
		}

		function get_same_day_event($id,$date,$status="",$room=""){
			if(is_numeric($date))
			{
				$date = date('Y-m-d',$date);
			}else{
				$date = date('Y-m-d',strtotime($date));
			}
			$this->db->where('date',"'".$date."'");

			if($status){
				$this->db->where('status',"'".$status."'");
			}

			if($room){
				$this->db->where('status',"'".$room."'");
			}

			$this->db->whereNot('id',$id);
			$event = $this->db->get('jdv_event');
			return $event;
		}
		
		public function insert_event($data){
			return $this->db->insert('jdv_event',$data);

		}

		public function update_event($id,$data){
			$this->db->where('id',$id);
			$this->db->update('jdv_event',$data);
		}

		public function validate_event_data($data) {

		}

		public function delete_event($id) {
			$this->db->where('id',$id);
			$this->db->delete('jdv_event','');
		}


		// Thread Comment Functions

		public function insert_comment($data) {
			return $this->db->insert('jdv_event_comment',$data);
		}

		public function get_comment($comment_id) {
			$this->db->where('id',$comment_id);
			$comment = $this->db->getSingle('jdv_event_comment');
			return $comment;
		}

		public function get_all_comments($event_id) {
			$this->db->where('event_id',$event_id);
			$this->db->orderBy('id','DESC');
			$comments = $this->db->get('jdv_event_comment');
			return $comments;
		}

		public function delete_comment($comment_id) {
			$this->db->where('id',$comment_id);
			$this->db->delete('jdv_event_comment','');
		}



	}

<?php

class BootstrapHelper extends LHelper{

	function __construct(){
		parent::__construct();
		$this->load->library('validation');
	}

	function errorClass($name){


		if($this->validation->getError($name))
			echo "has-error";
	}

	function inlineErrorMessage($name){
		if($this->validation->getError($name))
			echo "<span class='help-block error'>*".$this->validation->getError($name)."</span>";
	}

	function displayMessage(){
		$this->load->library('data');

		if($this->data->getFlash('message_exist')): 
  
			$class= $this->data->getFlash('type')
		?>
	
			<div class="alert <?php echo $class?>  fade in">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?php echo $this->data->getFlash('message') ?>
				<?php $this->validation->getAllError(4,true) ?> 
				
			</div>

		<?php
		else : ?>
	
		<?php
		endif; 
		
	}

	function tableSorter($column_name,$column_sort_name,$extra_rebulid_param=array()){
		$this->load->library('input');

		$order_by_type = $this->input->get('order_by_type');
		$order_by = $this->input->get('order_by');

		$label='';
	
		if($order_by==$column_sort_name){
		

			if($order_by_type=='desc'){
				$label='glyphicon glyphicon-chevron-down';
				$order_by_type='asc';

			}elseif($order_by_type=='asc'){
				$label='glyphicon glyphicon-chevron-up';
				$order_by_type='desc';
			}
			else{
				$order_by_type='asc';
			}
		}

		// not in order by position
		else{
			$order_by_type='asc';
		}

		if($label){
			$label = "<span class='table-sorter glyphicon $label'></span>";
		}


		//merge default rebuild param with extra rebuild param
		$extra_rebulid_param = array_merge(array('search','order_by','controller','method'),$extra_rebulid_param);

		$lotus = LotusFactory::getInstance();

		$this->load->library('URL');

		$segment = $this->uRL->getURLSegment(2);

		

		$base_url = l_base_url($lotus->getController()."/index/$segment",$extra_rebulid_param,array('order_by_type'=>$order_by_type,'order_by'=>$column_sort_name));

		$href = "<a href='$base_url'>$column_name </a> $label";

		

		echo $href;

	}

}
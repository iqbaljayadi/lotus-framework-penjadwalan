<?php

class JdvHelper extends LHelper{

	function __construct(){
		parent::__construct();
		$this->load->library('validation');
		$this->load->library('db');
	}

	function jdv_redirect_not_admin() {
		if(!current_user_can('install_plugins') || !is_user_logged_in()){
			wp_redirect(l_base_url());			
		}
	}

	function jdv_redirect_not_user() {
		if(current_user_can('install_plugins') || !is_user_logged_in()){
			wp_redirect(l_base_url());		
		}
	}


		// $type param: success, danger, info
	function alert($type='',$message) {
		$alert = '<div class="alert alert-'.$type.'">
		<button type="button" class="close" data-dismiss="alert">×</button>'.$message.'</div>';
		return $alert;
	}

	function form_start_time($time='',$param='') {
		$ret='';
		$ret.='<select name="start_time" '.$param.' required>';

		$start_time=array(
			"09:00:00","10:00:00","11:00:00","12:00:00","13:00:00","14:00:00","15:00:00",
			"16:00:00","17:00:00","18:00:00","19:00:00","20:00:00"
			);

		foreach ($start_time as $key => $start) {
			$sel = '';

			if($time==$start){
				$sel = 'selected';
			}

			$ret.='<option value="'.$start.'" '.$sel.'>';
			$ret.= date('H:i',strtotime($start));
			$ret.='</option>';
		}
		$ret.='</select>';
		echo $ret;
	}

	function form_end_time($time='',$param='') {
		$ret='';
		$ret.='<select name="end_time" '.$param.' required>';

		$start_time=array(
			"10:00:00","11:00:00","12:00:00","13:00:00","14:00:00","15:00:00",
			"16:00:00","17:00:00","18:00:00","19:00:00","20:00:00","21:00:00"
			);

		foreach ($start_time as $key => $start) {
			$sel = '';

			if($time==$start){
				$sel = 'selected';
			}

			$ret.='<option value="'.$start.'" '.$sel.'>';
			$ret.= date('H:i',strtotime($start));
			$ret.='</option>';
		}
		$ret.='</select>';
		echo $ret;
	}

	function form_event_status($event_status='',$param='') {
		$ret='';
		$ret.='<select name="status" '.$param.'>';
		$ret.='<option value=""></option>';

		$status=array(
			0,1,2
			);

		foreach ($status as $key => $stat) {

			$sel = '';
			$val = '';

			if($event_status==$stat && $event_status!=''){
				$sel = 'selected';
			}


			if($stat==0) {
				$val = 'Pending';
			}
			if($stat==1) {
				$val = 'Accepted';
			}
			if($stat==2) {
				$val = 'Rejected';
			}
			$ret.='<option value="'.$stat.'" '.$sel.'>';
			$ret.= $val;
			$ret.='</option>';
		}
		$ret.='</select>';
		echo $ret;
	}

	function display_event_status($stat='') {

		if($stat==0) {
			$val = 'Pending';
		}
		if($stat==1) {
			$val = 'Accepted';
		}
		if($stat==2) {
			$val = 'Rejected';
		}

		echo $val;
	}

	function get_event_status($stat='') {

		if($stat==0) {
			$val = 'Pending';
		}
		if($stat==1) {
			$val = 'Accepted';
		}
		if($stat==2) {
			$val = 'Rejected';
		}

		return $val;
	}

	function form_room($room_id='',$param='') {
		$room_list = $this->db->get('jdv_room');

		$ret='<select name="room" '.$param.'>';
		$ret.='<option value=""></option>';

		foreach ($room_list as $key => $room) {

			$sel = '';

			if($room_id==$room->id){
				$sel = 'selected';
			}

			$ret.='<option value="'.$room->id.'" '.$sel.'>';
			$ret.= $room->room_name;
			$ret.='</option>';
		}
		$ret.='</select>';
		echo $ret;
	}

	function display_room($room_id) {
		if($room_id) {
			$this->db->where('id',$room_id);
			$room = $this->db->getSingle('jdv_room');
			echo $room->room_name;
		}
	}

	function get_event_room($room_id) {
		if($room_id) {
			$this->db->where('id',$room_id);
			$room = $this->db->getSingle('jdv_room');
			return $room->room_name;
		}
	}

	function display_jdv_menu() {
		$menu_args = array(
			'theme_location'  => 'booking_menu',
			'menu'            => '',
			'container'       => 'div',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'menu',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'walker'          => ''
			);

		return wp_nav_menu( $menu_args );
	}
}

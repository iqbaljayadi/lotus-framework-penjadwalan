<?php

class MemberHelper extends LHelper{
	function get_user_data() {
		$id = get_current_user_id();
		return get_user_meta($id);
	}
}
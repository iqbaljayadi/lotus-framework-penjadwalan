<div class="si-container">
	<?php echo '<h1>'.$title.'</h1>'; ?>
	<?php echo $flash ?>
	<div>&nbsp;</div>
	<div class="row">
		<div class="col-sm-6">
			<span>Selamat datang di sistem booking event JDV. Untuk melanjutkan anda diharuskan login terlebih dahulu.</span>
			<div>&nbsp;</div>
			<form class="form-horizontal" name="loginform" id="loginform" action="<?php echo l_base_url('booking_login/login_process') ?>" method="post">
				<div class="form-group">	
					<label for="user_login" class="col-sm-3 control-label">JDV ID</label>
					<div class="col-sm-6">
						<input required type="text" class="form-control" id="user_login" class="input" name="log" size="25">
					</div>
				</div>

				<div class="form-group">	
					<label for="user_pass" class="col-sm-3 control-label">Password</label>
					<div class="col-sm-6">
						<input required type="password" class="form-control" id="user_pass" class="input" name="pwd">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<label><input name="rememberme" type="checkbox" id="rememberme" value="forever"> Remember Me</label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<input type="submit" class="btn btn-primary" name="wp-submit" id="wp-submit" value="Log In">
						<input type="hidden" name="redirect_to" value="http://localhost/wptest/si/booking_login/">
					</div>
				</div>

			</form>
			<div class="row">
				<div class="col-sm-7 col-sm-offset-3">
					<span>Belum memiliki akun booking? Register <a href="<?php echo l_base_url('booking_register') ?>">disini</a></span><br />
					<span><a href="<?php echo wp_lostpassword_url(); ?>" title="Lost Password">Lupa password?</a></span>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
		</div>
	</div>
</div>
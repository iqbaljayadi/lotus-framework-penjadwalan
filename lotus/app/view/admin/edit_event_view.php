<?php

echo '<a href="'.wp_logout_url(l_base_url()).'">Logout</a>';
echo '<h1>'.$title.'</h1>';
if($flash) {
	echo $flash;	
}

?>

<div id="si_admin" class="row">

	<?php if($event != NULL) { ?>
	<div class="col-sm-6">
		<form role="form" class="form-horizontal" action="<?php echo l_base_url('admin_event/update_event'); echo $event->id ?>" method="post">
			<div class="form-group">	
				<label for="event_name" class="col-sm-4 control-label">Event Name</label>
				<div class="col-sm-8">
					<input value="<?php echo $event->event_name; ?>" type="text" class="form-control" id="event_name" name="event_name" required>
				</div>
			</div>
			<div class="form-group">
				<label for="creator_name" class="col-sm-4 control-label">Name</label>
				<div class="col-sm-8">
					<input value="<?php echo $event->name; ?>" type="text" class="form-control" id="creator_name" name="creator_name"
					required>
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-4 control-label">Email</label>
				<div class="col-sm-8">
					<input value="<?php echo $event->email; ?>" type="email" class="form-control" id="email" name="email" required>
				</div>
			</div>
			<div class="form-group">
				<label for="phone" class="col-sm-4 control-label">Phone Number</label>
				<div class="col-sm-8">
					<input value="<?php echo $event->phone; ?>" type="tel" class="form-control" id="phone" name="phone" required>
				</div>
			</div>
			<div class="form-group">
				<label for="room" class="col-sm-4 control-label">Room</label>
				<div class="col-sm-8">
					<?php $this->jdvHelper->form_room($event->room) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="date" class="col-sm-4 control-label">Date</label>
				<div class="col-sm-4">
					<input value="<?php echo date('m/d/Y',strtotime($event->date)); ?>" type="text" class="form-control" id="date" name="date" required>
				</div>
			</div>
			<div class="form-group">
				<label for="Time" class="col-sm-4 control-label">Time</label>
				<div class="col-sm-6">
					<?php $this->jdvHelper->form_start_time($event->start_time) ?>
					&nbsp;
					To
					&nbsp;
					<?php $this->jdvHelper->form_end_time($event->end_time) ?>
				</div>
			</div>	
			<div class="form-group">
				<label for="participant" class="col-sm-4 control-label">Participants</label>
				<div class="col-sm-8">
					<input value="<?php echo $event->participant; ?>" type="number" step="1" min="1" class="form-control" id="participant" name="participant" required>
				</div>
			</div>	
			<div class="form-group">
				<label for="event_manager" class="col-sm-4 control-label">Manager</label>
				<div class="col-sm-8">
					<select name="event_manager" id="event_manager">
						<option value="Putro" <?php if($event->event_manager=='Putro'){echo 'selected';} ?>>Putro</option>
						<option value="Saga" <?php if($event->event_manager=='Saga'){echo 'selected';} ?>>Saga</option>
						<option value="Fira" <?php if($event->event_manager=='Fira'){echo 'selected';} ?>>Fira</option>
					</select>
				</div>
			</div>	
			<div class="form-group">
				<label for="status" class="col-sm-4 control-label" required>Status</label>
				<div class="col-sm-8">
					<?php $this->jdvHelper->form_event_status($event->status) ;?>	
				</div>
			</div>	
			<div class="form-group">
				<label for="notes" class="col-sm-4 control-label">Notes</label>
				<div class="col-sm-8">
					<textarea name="notes" style="width:100%"><?php echo $event->notes; ?></textarea>
				</div>
			</div>	
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-8">
					<button type="submit" class="btn btn-primary">Edit Event</button>
				</div>
			</div>
		</form>
	</div>
	
	<?php }

	else {
		echo '<div class="col-sm-12"><h3>No event found.</h3></div>';
	} ?>

</div>

<script type="text/javascript">
	jQuery(function($){
		$(function() {
			$( "#date" ).datepicker();
		});
	})
</script>
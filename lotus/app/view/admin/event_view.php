<div class="si-container">
	<div id="si-admin">
		<?php
		$this->jdvHelper->display_jdv_menu();

		echo '<a href="'.wp_logout_url(l_base_url()).'">Logout</a>';
		echo '<h1>'.$title.'</h1>';
		echo $flash;
		if (empty($_GET)) {
			$_GET['room']='';
			$_GET['start_date']=date('m/d/Y');
			$_GET['end_date']='';
			$_GET['event_name']='';
			$_GET['status']='';
		}

		?>

		<div id="event-panel-container" class="row">
			<div id="add-event" class="col-sm-2">
				<a href="<?php echo l_base_url('admin_event/new_event') ?>" class="btn btn-primary">Add New Event</a>
			</div>
			<div id="event-panel" class="col-sm-10">
				<form role="form" method="GET" class="form-horizontal">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">	
								<label for="event_name" class="col-sm-4 control-label">Event Name</label>
								<div class="col-sm-7">
									<input value="<?php echo $_GET['event_name'] ?>" type="text" class="form-control" id="event_name" name="event_name">
								</div>
							</div>
							<div class="form-group">	
								<label for="room" class="col-sm-4 control-label">Room</label>
								<div class="col-sm-8">
									<?php $this->jdvHelper->form_room($_GET['room']) ?>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4 col-sm-offset-4">
									<button type="submit" class="btn btn-info">Search</button>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group margintop10">
								<label for="date" class="col-sm-4 control-label">Start Date</label>
								<div class="col-sm-8">
									<input value="<?php echo date('m/d/Y',strtotime($_GET['start_date'])) ?>" type="text" class="form-control" id="start_date" name="start_date" class="datepicker">
								</div>
							</div>
							<div class="form-group">
								<label for="date" class="col-sm-4 control-label">End Date</label>
								<div class="col-sm-8">
									<input value="<?php if($_GET['end_date']) {echo date('m/d/Y',strtotime($_GET['end_date']));} ?>" type="text" class="form-control" id="end_date" name="end_date" class="datepicker">
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="status" class="col-sm-4 control-label">Status</label>
								<div class="col-sm-8">
									<?php $this->jdvHelper->form_event_status($_GET['status']) ?>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div id="event-list" class="row">
			<div class="col-sm-12">
				<?php if($events) { ?>
				<table>
					<thead>
						<tr>
							<th class="event-name">Event Name</th>
							<th class="name">Name</th>
							<th class="phone">Phone</th>
							<th class="room">Room</th>
							<th class="date">Date</th>
							<th class="time">Time</th>
							<th class="manager">Manager</th>
							<th class="status">Status</th>
							<th class="last-edit">Last Edit</th>
							<th class="action">Action</th>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($events as $key => $event) { ?>
						<tr>
							<td><a href="<?php echo l_base_url('admin_event/manage_event/'.$event->id) ?>" target="_blank"><?php echo $event->event_name ?></a></td>
							<td><?php echo $event->name ?></td>
							<td><?php echo $event->phone ?></td>
							<td><?php $this->jdvHelper->display_room($event->room) ?></td>
							<td>
								<?php
								echo date('d M Y', strtotime($event->date));
								?>
							</td>
							<td><?php 
								echo date('G.i',strtotime($event->start_time)).' to '.date('G.i',strtotime($event->end_time));
								?>
							</td>
							<td><?php echo $event->event_manager ?></td>
							<td><?php $this->jdvHelper->display_event_status($event->status) ?></td>
							<td><?php echo date('d M Y G.i',strtotime(str_replace('-','/',$event->last_edit))) ?></td>
							<td style="text-align:center;">
								<a href="<?php echo l_base_url('admin_event/manage_event/'.$event->id) ?>">Manage</a>
								<a href="<?php echo l_base_url('admin_event/edit_event/'.$event->id) ?>">Edit</a>
								<a onclick="return confirm('Anda yakin akan menghapus booking event ini?');" href="<?php echo l_base_url('admin_event/delete_event/'.$event->id) ?>">Delete</a>
							</td>
						</tr>
						<?php } ?>

					</tbody>
				</table>
				<?php }
				else  {
					echo '<h2>No event found.</h2>';
				} 		?>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	jQuery(function($){
		$(function() {
			$( "#start_date" ).datepicker();
			$( "#end_date" ).datepicker();
		});
	})
</script>
<div class="si-container">

	<?php

// Navigation
	$this->jdvHelper->display_jdv_menu();

	echo '<a href="'.wp_logout_url(l_base_url()).'">Logout</a><br />';	
	?>
	<a class="btn btn-info btn-mini" href="<?php echo l_base_url('admin_event') ?>">Back</a>

	<div id="si-admin" class="row">
		<div class="col-sm-7">
			<?php echo '<h1>'.$title.'</h1>';
			echo $flash;
			?>
		</div>
		<div class="col-sm-5">
			<h2>Contact Event Requester</h2>
		</div>
		<?php if($event != NULL) { ?>
		<div class="col-sm-7">
			<input type="hidden" value="<?php echo $event->id ?>" id="event-id" />
			<form role="form" class="form-horizontal" action="<?php echo l_base_url('admin_event/update_manage_event'); echo $event->id ?>" method="post">
				<div class="form-group">	
					<label for="event_name" class="col-sm-2 control-label">Event Name</label>
					<div class="col-sm-9">
						<input disabled value="<?php echo $event->event_name; ?>" type="text" class="form-control" id="event_name" name="event_name" required>
					</div>
				</div>
				<div class="form-group">
					<label for="creator_name" class="col-sm-2 control-label">Name</label>
					<div class="col-sm-9">
						<input disabled value="<?php echo $event->name; ?>" type="text" class="form-control" id="creator_name" name="creator_name"
						required>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-9">
						<input disabled value="<?php echo $event->email; ?>" type="email" class="form-control" id="email" name="email" required>
					</div>
				</div>
				<div class="form-group">
					<label for="phone" class="col-sm-2 control-label">Phone Number</label>
					<div class="col-sm-9">
						<input disabled value="<?php echo $event->phone; ?>" type="tel" class="form-control" id="phone" name="phone" required>
					</div>
				</div>
				<div class="form-group">
					<label for="room" class="col-sm-2 control-label">Room</label>
					<div class="col-sm-9">
						<?php $this->jdvHelper->form_room($event->room); ?>
					</div>
				</div>

				<div class="form-group">
					<label for="date" class="col-sm-2 control-label">Date</label>
					<div class="col-sm-4">
						<input value="<?php echo date('m/d/Y',strtotime($event->date)); ?>" type="text" class="form-control" id="date" name="date" class="datepicker" required>
					</div>
					<div class="col-sm-5">
						<?php  $this->jdvHelper->form_start_time($event->start_time); ?>
						&nbsp;To&nbsp;
						<?php  $this->jdvHelper->form_end_time($event->end_time); ?>
					</div>
				</div>
				<div id="conflicted-event">
					<?php if($event_conflict) { ?>
					<div class="row">
						<div id="list-same-schedule" class="col-sm-11">
							<span>There are <?php echo count($event_conflict) ?> events with same day schedule.</span>
							<div class="row">
								<?php foreach ($event_conflict as $key => $conflict) { ?>
								<div class="col-sm-12">
									<div class="same-schedule-item <?php $this->jdvHelper->display_event_status($conflict->status) ?>">
										<div class="row">
											<div class="col-sm-4 same-schedule-event-name">
												<a target="_blank" href="<?php echo l_base_url('admin_event/manage_event/'.$conflict->id) ?>"><?php echo $conflict->event_name ?></a>
											</div>
											<div class="col-sm-4 same-schedule-room">
												<?php $this->jdvHelper->display_room($conflict->room) ?>
											</div>
											<div class="col-sm-2 same-schedule-time">
												<?php echo date('G:i',strtotime($conflict->start_time)) ?>&nbsp;-&nbsp;<?php echo date('G:i',strtotime($conflict->end_time)) ?>
											</div>
											<div class="col-sm-2 same-schedule-status">
												<?php $this->jdvHelper->display_event_status($conflict->status) ?>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="form-group">
					<label for="participant" class="col-sm-2 control-label">Participants</label>
					<div class="col-sm-3">
						<input disabled value="<?php echo $event->participant; ?>" type="number" step="1" min="1" class="form-control" id="participant" name="participant" required>
					</div>
				</div>	
				<div class="form-group">
					<label for="event_manager" class="col-sm-2 control-label">Manager</label>
					<div class="col-sm-3">
						<select name="event_manager" id="event_manager">
							<option value="Putro" <?php if($event->event_manager=='Putro'){echo 'selected';} ?>>Putro</option>
							<option value="Saga" <?php if($event->event_manager=='Saga'){echo 'selected';} ?>>Saga</option>
							<option value="Fira" <?php if($event->event_manager=='Fira'){echo 'selected';} ?>>Fira</option>
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label for="status" class="col-sm-2 control-label" required>Status</label>
					<div class="col-sm-3">
						<?php  $this->jdvHelper->form_event_status($event->status) ;?>	
					</div>
				</div>	
				<div class="form-group">
					<label for="notes" class="col-sm-2 control-label">Notes</label>
					<div class="col-sm-8">
						<textarea rows="5" disabled name="notes" style="width:100%"><?php echo $event->notes; ?></textarea>
					</div>
				</div>	
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<button type="submit" class="btn btn-primary">Update Event</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-5">
			<div id="comment-alert"></div>
			<form role="form" class="form" action="<?php // echo l_base_url('admin_event/submit_comment'); echo $event->id ?>" method="post">
				<div class="form-group">
					<textarea rows="4" name="comment" id="comment" style="width:100%" placeholder="Your message here..."></textarea>
				</div>	
				<div class="form-group">
					<div class="btn btn-info" id="submit-comment">Submit</div>
				</div>
			</form>
			<ul id="comment-container">
				<?php if($comments) { ?>

				<?php foreach ($comments as $key => $comment) { ?>
				<li class="comment-item-<?php echo $comment->id ?>">
					<div class='comment-identity'>

						<span class='comment-name'>
							<?php
							$user_info = get_userdata($comment->user_id);
							echo $user_info->first_name." ".$user_info->last_name;
							?>
						</span>
						&nbsp;
						<span class='comment-time'>
							<?php echo date('F j, Y H.i',strtotime($comment->timestamp)) ?>
						</span>
					</div>
					<div class='comment-content'>
						<?php echo $comment->comment ?>
					</div>
					<div class='comment-delete'>
						<?php if($comment->user_id == get_current_user_id()) { ?>
						<div class='btn btn-mini btn-warning delete'>Delete</div>
						<?php } ?>
					</div>
				</li>
				<?php } } ?>
			</ul>
		</div>

		<?php }

		else {
			wp_redirect(l_base_url('admin_event'));
		} ?>

	</div>

</div>

<script type="text/javascript">
	jQuery(function($){
		$(function() {
			$( "#date" ).datepicker();
		});

		$('#date').change(function(){
			var id 		=  $('#event-id').val();
			var date 	= $('#date').val();

			$.ajax({
				url: '<?php echo l_base_url("ajax/get_conflicted_events") ?>',
				data: {id : id,date:date},
				type: "POST",
				dataType: "json",
				success: function(output){
					$('#conflicted-event').html(output)
				}
			});
		})

		$('#submit-comment').click(function(){
			var event_id 		=  $('#event-id').val();
			var comment 	= $('#comment').val();
			var check = comment;
			if(check.trim()) {

				$(this).addClass('disabled');
				$(this).html('Loading');

				$.ajax({
					url: '<?php echo l_base_url("ajax/submit_comment") ?>',
					data: {event_id : event_id,
						comment : comment
					},
					type: "POST",
					dataType: "json",
					success: function(output){

						$('#comment-alert').html('<div class="alert alert-'+output.type+'">' + output.alert +'<button type="button" class="close" data-dismiss="alert">×</button></div>');
						$('#comment-container').prepend(output.data);
						$('#submit-comment').removeClass('disabled');
						$('#submit-comment').html('Submit');
						$('#comment').val('');
					}
				});
			}

			else {
				alert('Kolom pesan tidak boleh kosong.');
			}
		})


		$('.delete').click(function(){
			
			if(confirm('Anda yakin akan menghapus pesan ini?')) {
				var event_id	= $('#event-id').val();
				var comment_id	= parseInt($(this).closest('li').attr('class').match(/\d+/),10);

				var el = $(this);

				$(this).addClass('disabled');
				$(this).html('loading');

				$.ajax({
					url: '<?php echo l_base_url("ajax/delete_comment") ?>',
					data: {event_id : event_id,comment_id : comment_id},
					type: "POST",
					dataType: "json",
					success: function(output){
						$('#comment-alert').html('<div class="alert alert-'+output.type+'">' + output.alert +'<button type="button" class="close" data-dismiss="alert">×</button></div>');
						if(output.type=='success') {
							$(el).closest('li').remove();
						}

						else {
							$(el).removeClass('disabled');
							$(el).html('Delete');
						}
					}
				});
			}
		})
	})
</script>
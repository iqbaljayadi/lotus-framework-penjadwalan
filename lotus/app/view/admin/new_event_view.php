<div class="si-container">
	<?php

	echo '<a href="'.wp_logout_url(l_base_url()).'">Logout</a>';
	?>
	<br />
	<a class="btn btn-info btn-mini" href="<?php echo l_base_url('admin_event') ?>">Back</a>
	<?php
	echo $flash;	
	echo '<h1>'.$title.'</h1>';
	?>
	<div id="si_admin" class="row">
		<div class="col-sm-6">
			<form role="form" class="form-horizontal" action="<?php echo l_base_url('admin_event/submit_event') ?>" method="post">
				<div class="form-group">
					<label for="event_name" class="col-sm-4 control-label">Event Name</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="event_name" name="event_name" required>
					</div>
				</div>
				<div class="form-group">
					<label for="creator_name" class="col-sm-4 control-label">Name</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="creator_name" name="creator_name"
						value="<?php echo $usermeta['first_name'][0] ?> <?php echo $usermeta['last_name'][0] ?>" required>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-4 control-label">Email</label>
					<div class="col-sm-8">
						<input type="email" class="form-control" id="email" name="email" required>
					</div>
				</div>
				<div class="form-group">
					<label for="phone" class="col-sm-4 control-label">Phone Number</label>
					<div class="col-sm-8">
						<input type="tel" class="form-control" id="phone" name="phone" required>
					</div>
				</div>
				<div class="form-group">
					<label for="room" class="col-sm-4 control-label">Room</label>
					<div class="col-sm-8">
						<?php $this->jdvHelper->form_room() ?>
					</div>
				</div>
				<div class="form-group">
					<label for="date" class="col-sm-4 control-label">Date</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="date" name="date" required class="datepicker">
					</div>
				</div>
				<div class="form-group">
					<label for="Time" class="col-sm-4 control-label">Time</label>
					<div class="col-sm-8">
						<?php $this->jdvHelper->form_start_time() ?>
						&nbsp;To&nbsp;
						<?php $this->jdvHelper->form_end_time() ?>
					</div>
				</div>	
				<div class="form-group">
					<label for="participant" class="col-sm-4 control-label">Participants</label>
					<div class="col-sm-8">
						<input type="number" step="1" min="1" class="form-control" id="participant" name="participant" required>
					</div>
				</div>	
				<div class="form-group">
					<label for="event_manager" class="col-sm-4 control-label">Manager</label>
					<div class="col-sm-8">
						<!-- <input type="text" class="form-control" id="event_manager" name="event_manager"> -->
						<select name="event_manager" id="event_manager">
							<option value="Putro">Putro</option>
							<option value="Saga">Saga</option>
							<option value="Fira">Fira</option>
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label for="status" class="col-sm-4 control-label" required>Status</label>
					<div class="col-sm-8">
						<select name="status">
							<option value="0" selected>Pending</option>
							<option value="1">Accepted</option>
							<option value="2">Rejected</option>
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label for="notes" class="col-sm-4 control-label">Notes</label>
					<div class="col-sm-8">
						<textarea rows="5" name="notes" style="width:100%">Your notes here...</textarea>
					</div>
				</div>	
				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-8">
						<button type="submit" class="btn btn-default">Create Event</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-12">
			<!-- chat sessiong -->
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(function($){
		$(function() {
			$( "#date" ).datepicker();
		});
	});
</script>
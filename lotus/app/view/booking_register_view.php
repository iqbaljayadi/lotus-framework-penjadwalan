<div class="si-container">
	<a href="<?php echo l_base_url('booking_login') ?>">Ke halaman login</a>
	<?php echo '<h1>'.$title.'</h1>';
	echo $flash;
	if($errflash) {
		foreach ($errflash as $key => $err) {
			echo $err;
		}
	}

	?>
	<div class="row">
		<div class="col-sm-12">
			<form class="form-horizontal" onsubmit="return validatePassword()" name="registerform" id="registerform" action="<?php echo l_base_url('booking_register/register') ?>" method="post">

				<div class="form-group">	
					<label for="jdv_id" class="col-sm-1 control-label">JDV ID</label>
					<div class="col-sm-5">
						<input required type="text" class="form-control" id="jdv_id" class="input" name="jdv_id" size="25">
					</div>
					<div class="col-sm-5">
						<span>Masukkan angka sesuai ID yang tertera pada kartu member JDV anda</span>
					</div>
				</div>

				<div class="form-group">	
					<label for="email" class="col-sm-1 control-label">Email</label>
					<div class="col-sm-5">
						<input required type="email" class="form-control" id="email" class="input" name="email" size="35">
					</div>
					<div class="col-sm-5">
						Email harus sama dengan email membership JDV anda
					</div>
				</div>

				<div class="form-group">	
					<label for="password" class="col-sm-1 control-label">password</label>
					<div class="col-sm-5">
						<input required type="password" class="form-control" id="password" class="input" name="password">
					</div>
				</div>

				<div class="form-group">	
					<label for="passwordvalidate" class="col-sm-1 control-label">Validate password</label>
					<div class="col-sm-5">
						<input required type="password" class="form-control" id="passwordvalidate" class="input" name="passwordvalidate">
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-5 col-sm-offset-1">
						<input type="submit" name="wp-submit" id="wp-submit" class="btn btn-primary" value="Register">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-8 col-sm-offset-1">
						Pastikan anda telah menjadi member Jogja Digital Valley untuk dapat melakukan registrasi akun booking.<br/>
						Informasi pendaftaran membership ada <a href="#">disini</a>.
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	function validatePassword() {
		var pass1 = document.getElementById("password").value;
		var pass2 = document.getElementById("passwordvalidate").value;
		var ok = true;
		if (pass1 != pass2) {
      	//alert("Passwords Do not match");
      	document.getElementById("password").style.borderColor = "#E34234";
      	document.getElementById("passwordvalidate").style.borderColor = "#E34234";
      	ok = false;
      	alert("Password yang diisikan harus sama.");
      }

      return ok;
  }
</script>
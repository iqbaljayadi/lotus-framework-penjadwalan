<div class="si-container">
	<nav>
		<ul id="menu-booking-menu">
			<li><a href="#">My Events</a></li>
			<li><a href="<?php echo wp_logout_url(l_base_url()); ?>">Log Out</a></li>
		</ul>
	</nav>
	<?php
	echo '<a href="'.(l_base_url('user_booking')).'" class="btn btn-info">Back</a>';
	?>

	<div id="si-user" class="row">
		<div class="col-sm-7">
			<?php echo '<h1>'.$title.'</h1>'; ?>
		</div>
		<div class="col-sm-5">
			<h2>Contact Admin</h2>
		</div>
		<?php if($event != NULL) { ?>
		<div class="col-sm-7">
			<?php echo $flash; ?>
			<input type="hidden" value="<?php echo $event->id ?>" id="event-id" />
			<form role="form" class="form-horizontal" action="<?php echo l_base_url('user_booking/update_manage_booking'); echo $event->id ?>" method="post">
				<div class="form-group">	
					<label for="event_name" class="col-sm-2 control-label">Event Name</label>
					<div class="col-sm-9">
						<input value="<?php echo $event->event_name; ?>" type="text" class="form-control" id="event_name" name="event_name" required>
					</div>
				</div>
				<div class="form-group">
					<label for="creator_name" class="col-sm-2 control-label">Name</label>
					<div class="col-sm-9">
						<input disabled value="<?php echo $event->name; ?>" type="text" class="form-control" id="creator_name" name="creator_name"
						required>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-9">
						<input disabled value="<?php echo $event->email; ?>" type="email" class="form-control" id="email" name="email" required>
					</div>
				</div>
				<div class="form-group">
					<label for="phone" class="col-sm-2 control-label">Phone Number</label>
					<div class="col-sm-9">
						<input value="<?php echo $event->phone; ?>" type="tel" class="form-control" id="phone" name="phone" required>
					</div>
				</div>
				<div class="form-group">
					<label for="room" class="col-sm-2 control-label">Room</label>
					<div class="col-sm-9">
						<?php $this->jdvHelper->form_room($event->room,'disabled'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="date" class="col-sm-2 control-label">Date</label>
					<div class="col-sm-4">
						<input disabled value="<?php echo date('m/d/Y',strtotime($event->date)); ?>" type="text" class="form-control" id="date" name="date" class="datepicker">
					</div>
					<div class="col-sm-5">
						<?php  $this->jdvHelper->form_start_time($event->start_time,'disabled'); ?>
						&nbsp;To&nbsp;
						<?php  $this->jdvHelper->form_end_time($event->end_time,'disabled'); ?>
					</div>
				</div>

				<div class="form-group">
					<label for="participant" class="col-sm-2 control-label">Participants</label>
					<div class="col-sm-3">
						<input value="<?php echo $event->participant; ?>" type="number" step="1" min="1" class="form-control" id="participant" name="participant" required>
					</div>
				</div>	
				<div class="form-group">
					<label for="event_manager" class="col-sm-2 control-label">Manager</label>
					<div class="col-sm-3">
						<input disabled value="<?php echo $event->event_manager; ?>" type="text" class="form-control" id="event_manager" name="event_manager">
					</div>
				</div>	
				<div class="form-group">
					<label for="status" class="col-sm-2 control-label" required>Status</label>
					<div class="col-sm-3">
						<?php  $this->jdvHelper->form_event_status($event->status,'disabled') ;?>	
					</div>
				</div>	
				<div class="form-group">
					<label for="notes" class="col-sm-2 control-label">Notes</label>
					<div class="col-sm-8">
						<textarea rows="5" name="notes" style="width:100%"><?php echo $event->notes; ?></textarea>
					</div>
				</div>	
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<button type="submit" class="btn btn-success">Update Event</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-5">
			<div id="comment-alert"></div>
			<form role="form" class="form" action="<?php // echo l_base_url('user_booking/submit_comment'); echo $event->id ?>" method="post">
				<div class="form-group">
					<textarea rows="4" name="comment" id="comment" style="width:100%" placeholder="Kirim pesan berupa pertanyaan, konfirmasi, pergantian jadwal, dan lain-lain kepada admin Jogja Digital Valley disini..."></textarea>
				</div>	
				<div class="form-group">
					<div class="btn btn-info" id="submit-comment">Submit</div>
				</div>
			</form>
			<ul id="comment-container">
				<?php if($comments) { ?>

				<?php foreach ($comments as $key => $comment) { ?>
				<li class="comment-item-<?php echo $comment->id ?>">
					<div class='comment-identity'>

						<span class='comment-name'>
							<?php
							$user_info = get_userdata($comment->user_id);
							echo $user_info->first_name." ".$user_info->last_name;
							?>
						</span>
						&nbsp;
						<span class='comment-time'>
							<?php echo date('F j, Y H.i',strtotime($comment->timestamp)) ?>
						</span>
					</div>
					<div class='comment-content'>
						<?php echo $comment->comment ?>
					</div>
					<div class='comment-delete'>
						<?php if($comment->user_id == get_current_user_id()) { ?>
						<div class='btn btn-mini btn-warning delete'>Delete</div>
						<?php } ?>
					</div>

				</li>
				<?php } }?>
			</ul>
		</div>

		<?php }

		else {
			wp_redirect(l_base_url('user_booking'));
		}?>
	</div>
</div>

<script type="text/javascript">
	jQuery(function($){
		$(function() {
			$( "#date" ).datepicker();
		});

		$('#submit-comment').click(function(){
			var event_id 		=  $('#event-id').val();
			var comment 	= $('#comment').val();
			var check = comment;
			if(check.trim()) {

				$(this).addClass('disabled');
				$(this).html('Loading');

				$.ajax({
					url: '<?php echo l_base_url("ajax/submit_comment") ?>',
					data: {event_id : event_id,
						comment : comment
					},
					type: "POST",
					dataType: "json",
					success: function(output){

						$('#comment-alert').html('<div class="alert alert-'+output.type+'">' + output.alert +'<button type="button" class="close" data-dismiss="alert">×</button></div>');
						$('#comment-container').prepend(output.data);
						$('#submit-comment').removeClass('disabled');
						$('#submit-comment').html('Submit');
						$('#comment').val('');
					}
				});
			}

			else {
				alert('Kolom pesan tidak boleh kosong.');
			}
		})


		$('.delete').click(function(){
			
			if(confirm('Anda yakin akan menghapus pesan ini?')) {
				var event_id	= $('#event-id').val();
				var comment_id	= parseInt($(this).closest('li').attr('class').match(/\d+/),10);

				var el = $(this);

				$(this).addClass('disabled');
				$(this).html('loading');

				$.ajax({
					url: '<?php echo l_base_url("ajax/delete_comment") ?>',
					data: {event_id : event_id,comment_id : comment_id},
					type: "POST",
					dataType: "json",
					success: function(output){
						$('#comment-alert').html('<div class="alert alert-'+output.type+'">' + output.alert +'<button type="button" class="close" data-dismiss="alert">×</button></div>');
						if(output.type=='success') {
							$(el).closest('li').remove();
						}

						else {
							$(el).removeClass('disabled');
							$(el).html('Delete');
						}
					}
				});
			}
		})


	})
</script>
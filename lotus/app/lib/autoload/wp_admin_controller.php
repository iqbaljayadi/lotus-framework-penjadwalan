<?php

class WPAdminController extends LController{
	function __construct(){
		parent::__construct();
		
		if(!is_admin() || !is_user_logged_in()){
			l_base_redirect('');			
		}
	}

}
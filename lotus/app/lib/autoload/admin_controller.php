<?php

class AdminController extends LController {
	function __construct(){
		parent::__construct();
		if(!is_user_logged_in() || !is_admin())
		{
			wp_redirect(home_url() .'/login');
		}
	}

	// function index() {
	// 	if(!current_user_can('install_plugins')){
	// 		wp_redirect(l_base_url('admin/event'));
	// 	}

	// 	else {
	// 		wp_redirect(l_base_url());
	// 	}
	// }
}
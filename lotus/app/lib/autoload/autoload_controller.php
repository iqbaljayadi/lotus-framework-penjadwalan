<?php

class AutoloadController extends WPFrontEndController{
	protected $user;
	function __construct(){
		parent::__construct();
		//WordPress default function
		date_default_timezone_set("Asia/Jakarta"); // set timezone

		/* inject javascrit untuk upload media yang bisa langsung akses media di wordpress*/
		wp_enqueue_media();

		/* Memanggil template files css dan javascript untuk front-end */
		$plugin_url = plugins_url( 'tonjoo-lotus-bootstraper' );
		$css = $plugin_url.'/assets/css/style.css';
		$js = $plugin_url . '/assets/js/tonjoo-lotus-media-upload.js';
		wp_register_script('tonjoo-lotus-media-upload-js', $js);
		wp_enqueue_script('tonjoo-lotus-media-upload-js');
		wp_register_style('information-system-style',$css);
		wp_enqueue_style('information-system-style');

		$jquery_ui_js = $plugin_url.'/assets/js/jquery-ui.js';
		$jquery_ui_css = $plugin_url.'/assets/css/jquery-ui.css';

		wp_register_script('jquery-ui-js', $jquery_ui_js);
		wp_enqueue_script('jquery-ui-js');
		wp_register_style('jquery-ui-css',$jquery_ui_css);
		wp_enqueue_style('jquery-ui-css');

		/* Load library dan helper yang disediakan Lotus Framework */
		$this->load->library('data');
		$this->load->library('input');
		$this->load->library('validation');
		$this->load->helper('jdv');

		// $this->load->library('db');

	// Custom Autoload Functions

		/// Global Variable for user data
		$id = get_current_user_id();
		$this->userdata = get_userdata($id);
		$this->usermeta = get_user_meta($id);
	}
}
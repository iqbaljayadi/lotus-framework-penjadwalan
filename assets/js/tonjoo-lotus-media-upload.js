jQuery(document).ready(function($){
	
    var custom_uploader
    var media_button
	/* id lotusmediauploadbutton untuk memanggil funsi js tersebut*/
    $('#lotusmediauploadbutton').click(function(e) {
        media_button = $(this)
        e.preventDefault();
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });
        custom_uploader.on('select', function() {
            /* mengambil url updload */
			attachment 			= custom_uploader.state().get('selection').first().toJSON();
            thumbnail 			= attachment.url
			/* parsing nama url*/
			var arra 			= thumbnail.split('/');
			var name 			= arra[8].split('.');
			var newname 		= name[0]+'-150x150.'+ name[1];
			var newThumnails 	= arra[0] + '//' + arra[1]  + '/' + arra[2] + '/' + arra[3] + '/' + arra[4] + '/' + arra[5] + '/' + arra[6] + '/' + arra[7] + '/' + newname;
			var encoded = encodeURIComponent(newThumnails);
			/* append data hasil upload berupa thiumbnails */
			$('#imageAppend').append('<div class="img-wrap">'
									+'<span class="close">&times;</span>'
									+'<input type="hidden"  name="additionalImage[]" value="'+newThumnails+'">'
                                	+'<img src="'+newThumnails+'" alt="sawijining gambar" class="img-rounded img-responsive" data-id="103">'
									+'</div>');
									
			/* menghapus image yang di append */
			$('.close').on('click', function(e) {
				e.preventDefault();
				var imgsa = $(this).parent().html();
				$(this).parent().remove();
			});
        });
        custom_uploader.open();
    });
});
//if call this js <div id="lotusmediauploadbutton"></div>